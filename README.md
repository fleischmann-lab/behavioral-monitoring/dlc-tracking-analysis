# Prerequisites 🐍

The only prerequisite is to install the Conda package manager. There are two options to do that:
* [The Anaconda distribution](https://www.anaconda.com/products/individual), which is a huge full featured distribution with many things included, like a graphical interface.
* [The Miniconda distribution](https://docs.conda.io/en/latest/miniconda.html), which is a leaner distribution, and is command line only.

Miniconda is good if you know your way around the command line. Otherwise I would suggest to choose the Anaconda distribution.

# Setup

To create your environment from scratch, open a terminal
```
conda env create -f environment.yml
```
Then activate your environment:
```
conda activate DLC_analysis
```

Add your new environment (kernel) in Jupyter:
```
python -m ipykernel install --user --name=DLC_analysis
```

To make the extensions work in JupyterLab:
```
jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-matplotlib @jupyterlab/debugger
```
## Git hooks ❌ ✔️
The CI will run several checks on the new code pushed to the repository. These checks can also be run locally without waiting for the CI by following the steps below:
1. [Install `pre-commit`](https://pre-commit.com/): it should already be installed with your environment, if not, update your environment or install it separately.
2. Install the Git hooks by running `pre-commit install`.

Once those two steps are done, the Git hooks will be run automatically at every new commit. The Git hooks can also be run manually with `pre-commit run --all-files`, and if needed they can be skipped (not recommended) with `git commit --no-verify`.

# Time to start working finally 👷

If you made it this far, now you can run JupyterLab 🚀 with the following command:
```
jupyter lab
```

Once in JupyterLab, don't forget to change your kernel to DLC_analysis.

If you later need to add new packages to your environment, just list them in the `environment.yml` file, and then update your environment with:
```
conda env update -f environment.yml
```
